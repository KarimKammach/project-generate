'use strict';
/**
 * Module dependencies.
 */
var glob = require('glob'), chalk = require('chalk');
/**
 * Module init function.
 */
module.exports = function () {
    /**
     * Before we begin, lets set the environment variable
     * We'll Look for a valid NODE_ENV variable and if one cannot be found load the development NODE_ENV
     */
    let configPath = '/app/config/env/';
    if (process.env.NODE_ENV === 'local') {
        configPath = './config/env/';
    }
    glob(configPath + process.env.NODE_ENV + '.js', function (err, environmentFiles) {
        if (!environmentFiles.length) {
            if (process.env.NODE_ENV) {
                console.error(chalk.red('No configuration file found for "' + process.env.NODE_ENV + '" environment using development instead'));
            }
            else {
                console.error(chalk.red('NODE_ENV is not defined! Using default development environment'));
            }
            process.env.NODE_ENV = 'development';
        }
        else {
            console.log(chalk.black.bgWhite('Application loaded using the "' + process.env.NODE_ENV + '" environment configuration'));
        }
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdC5qcyIsInNvdXJjZVJvb3QiOiJhcHAvIiwic291cmNlcyI6WyJjb25maWcvaW5pdC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxZQUFZLENBQUM7QUFFYjs7R0FFRztBQUNILElBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFDekIsS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUUxQjs7R0FFRztBQUNILE1BQU0sQ0FBQyxPQUFPLEdBQUc7SUFDaEI7OztPQUdHO0lBQ0gsSUFBSSxVQUFVLEdBQUcsa0JBQWtCLENBQUM7SUFDakMsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsS0FBSyxPQUFPLEVBQUU7UUFDbEMsVUFBVSxHQUFHLGVBQWUsQ0FBQztLQUNoQztJQUNKLElBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsS0FBSyxFQUFFLFVBQVMsR0FBRyxFQUFFLGdCQUFnQjtRQUM3RSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFO1lBQzdCLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3pCLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxtQ0FBbUMsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBRyx5Q0FBeUMsQ0FBQyxDQUFDLENBQUM7YUFDakk7aUJBQU07Z0JBQ04sT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLGdFQUFnRSxDQUFDLENBQUMsQ0FBQzthQUMzRjtZQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFHLGFBQWEsQ0FBQztTQUNyQzthQUFNO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxnQ0FBZ0MsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBRyw2QkFBNkIsQ0FBQyxDQUFDLENBQUM7U0FDMUg7SUFDRixDQUFDLENBQUMsQ0FBQztBQUVKLENBQUMsQ0FBQyJ9