'use strict';
module.exports = {
    db: 'mongodb://localhost:30000/shargo-test',
    sauronUrl: "https://sauron-test.shargo.io",
    apiUrl: "https://api-test.shargo.io",
    cronUrl: "https://cron-test.shargo.io",
    webUrl: "https://web-test.shargo.io",
    port: 3001,
    app: {
        title: 'Shargo - Test Environment'
    },
    payment: {
        merchantId: "22ygpbzkc37bvsqc",
        publicKey: "x6q6tg37pyhgwd6b",
        privateKey: "3f5dcf8906deb9acab5392308bf51c62"
    },
    emitter: {
        isEnabled: false,
        url: "http://data-recruiter.shargo.io:5500",
        auth: {
            user: "shargo-data-server",
            pass: "pack3tb3ll.15"
        }
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVzdC5qcyIsInNvdXJjZVJvb3QiOiJhcHAvIiwic291cmNlcyI6WyJjb25maWcvZW52L3Rlc3QuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsWUFBWSxDQUFDO0FBRWIsTUFBTSxDQUFDLE9BQU8sR0FBRztJQUNoQixFQUFFLEVBQUUsdUNBQXVDO0lBQzNDLFNBQVMsRUFBRSwrQkFBK0I7SUFDMUMsTUFBTSxFQUFFLDRCQUE0QjtJQUNwQyxPQUFPLEVBQUUsNkJBQTZCO0lBQ3RDLE1BQU0sRUFBRSw0QkFBNEI7SUFDcEMsSUFBSSxFQUFFLElBQUk7SUFDVixHQUFHLEVBQUU7UUFDSixLQUFLLEVBQUUsMkJBQTJCO0tBQ2xDO0lBQ0QsT0FBTyxFQUFFO1FBQ1IsVUFBVSxFQUFHLGtCQUFrQjtRQUMvQixTQUFTLEVBQUksa0JBQWtCO1FBQy9CLFVBQVUsRUFBRyxrQ0FBa0M7S0FDL0M7SUFDRCxPQUFPLEVBQUU7UUFDUixTQUFTLEVBQUUsS0FBSztRQUNoQixHQUFHLEVBQUUsc0NBQXNDO1FBQzNDLElBQUksRUFBRTtZQUNMLElBQUksRUFBRSxvQkFBb0I7WUFDMUIsSUFBSSxFQUFFLGVBQWU7U0FDckI7S0FDRDtDQUNELENBQUMifQ==