'use strict';
module.exports = {
    app: {
        title: 'Shargo',
        description: 'Sending packages super fast',
        keywords: 'Shargo, packages'
    },
    port: process.env.PORT || 8000,
    securePort: 443,
    templateEngine: 'swig',
    secret: 'LAfang.2015PB'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxsLmpzIiwic291cmNlUm9vdCI6ImFwcC8iLCJzb3VyY2VzIjpbImNvbmZpZy9lbnYvYWxsLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLFlBQVksQ0FBQztBQUViLE1BQU0sQ0FBQyxPQUFPLEdBQUc7SUFDaEIsR0FBRyxFQUFFO1FBQ0osS0FBSyxFQUFFLFFBQVE7UUFDZixXQUFXLEVBQUUsNkJBQTZCO1FBQzFDLFFBQVEsRUFBRSxrQkFBa0I7S0FDNUI7SUFDRCxJQUFJLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksSUFBSTtJQUM5QixVQUFVLEVBQUUsR0FBRztJQUNmLGNBQWMsRUFBRSxNQUFNO0lBQ3RCLE1BQU0sRUFBRSxlQUFlO0NBQ3ZCLENBQUMifQ==