'use strict';
module.exports = {
    db: 'mongodb://shargo:test1234@128.199.38.91:37017/shargo',
    sauronUrl: "https://sauron-local.shargo.io",
    apiUrl: "https://api-local.shargo.io",
    cronUrl: "https://cron-local.shargo.io",
    webUrl: "https://web-local.shargo.io",
    app: {
        title: 'Shargo - Development Environment'
    },
    payment: {
        merchantId: "22ygpbzkc37bvsqc",
        publicKey: "x6q6tg37pyhgwd6b",
        privateKey: "3f5dcf8906deb9acab5392308bf51c62"
    },
    emitter: {
        isEnabled: false,
        url: "http://data-recruiter.shargo.io:5500",
        auth: {
            user: "shargo-data-server",
            pass: "pack3tb3ll.15"
        }
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWwuanMiLCJzb3VyY2VSb290IjoiYXBwLyIsInNvdXJjZXMiOlsiY29uZmlnL2Vudi9sb2NhbC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxZQUFZLENBQUM7QUFFYixNQUFNLENBQUMsT0FBTyxHQUFHO0lBQ2hCLEVBQUUsRUFBRSxzREFBc0Q7SUFDMUQsU0FBUyxFQUFFLGdDQUFnQztJQUMzQyxNQUFNLEVBQUUsNkJBQTZCO0lBQ3JDLE9BQU8sRUFBRSw4QkFBOEI7SUFDdkMsTUFBTSxFQUFFLDZCQUE2QjtJQUNyQyxHQUFHLEVBQUU7UUFDSixLQUFLLEVBQUUsa0NBQWtDO0tBQ3pDO0lBQ0QsT0FBTyxFQUFFO1FBQ1IsVUFBVSxFQUFHLGtCQUFrQjtRQUMvQixTQUFTLEVBQUksa0JBQWtCO1FBQy9CLFVBQVUsRUFBRyxrQ0FBa0M7S0FDL0M7SUFDRCxPQUFPLEVBQUU7UUFDUixTQUFTLEVBQUUsS0FBSztRQUNoQixHQUFHLEVBQUUsc0NBQXNDO1FBQzNDLElBQUksRUFBRTtZQUNMLElBQUksRUFBRSxvQkFBb0I7WUFDMUIsSUFBSSxFQUFFLGVBQWU7U0FDckI7S0FDRDtDQUNELENBQUMifQ==