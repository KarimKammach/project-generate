'use strict';
module.exports = {
    db: 'mongodb://shargo:test1234@db.shargo.io/shargo',
    sauronUrl: "https://sauron-sandbox.shargo.io",
    apiUrl: "https://api-sandbox.shargo.io",
    cronUrl: "https://cron-sandbox.shargo.io",
    webUrl: "https://web-sandbox.shargo.io",
    app: {
        title: 'Shargo - Development Environment'
    },
    payment: {
        merchantId: "22ygpbzkc37bvsqc",
        publicKey: "x6q6tg37pyhgwd6b",
        privateKey: "3f5dcf8906deb9acab5392308bf51c62"
    },
    emitter: {
        isEnabled: false,
        url: "http://data-recruiter.shargo.io:5500",
        auth: {
            user: "shargo-data-server",
            pass: "pack3tb3ll.15"
        }
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGV2ZWxvcG1lbnQuanMiLCJzb3VyY2VSb290IjoiYXBwLyIsInNvdXJjZXMiOlsiY29uZmlnL2Vudi9kZXZlbG9wbWVudC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxZQUFZLENBQUM7QUFFYixNQUFNLENBQUMsT0FBTyxHQUFHO0lBQ2hCLEVBQUUsRUFBRSwrQ0FBK0M7SUFDbkQsU0FBUyxFQUFFLGtDQUFrQztJQUM3QyxNQUFNLEVBQUUsK0JBQStCO0lBQ3ZDLE9BQU8sRUFBRSxnQ0FBZ0M7SUFDekMsTUFBTSxFQUFFLCtCQUErQjtJQUN2QyxHQUFHLEVBQUU7UUFDSixLQUFLLEVBQUUsa0NBQWtDO0tBQ3pDO0lBQ0QsT0FBTyxFQUFFO1FBQ1IsVUFBVSxFQUFHLGtCQUFrQjtRQUMvQixTQUFTLEVBQUksa0JBQWtCO1FBQy9CLFVBQVUsRUFBRyxrQ0FBa0M7S0FDL0M7SUFDRCxPQUFPLEVBQUU7UUFDUixTQUFTLEVBQUUsS0FBSztRQUNoQixHQUFHLEVBQUUsc0NBQXNDO1FBQzNDLElBQUksRUFBRTtZQUNMLElBQUksRUFBRSxvQkFBb0I7WUFDMUIsSUFBSSxFQUFFLGVBQWU7U0FDckI7S0FDRDtDQUNELENBQUMifQ==