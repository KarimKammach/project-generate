'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const queryParser_1 = require("../app/middleware/queryParser");
var express = require('express'), bodyParser = require('body-parser'), compression = require('compression'), methodOverride = require('method-override'), mongoose = require('mongoose'), fs = require('fs'), cors = require('cors'), path = require('path'), consolidate = require('consolidate'), config = require('./config');
// raygun = require('raygun'),
// raygunClient = new raygun.Client().init({ apiKey: 'g3QH5dkSIFHk5WmQQ/uquQ==' });
module.exports = function () {
    var app = express();
    var hidePoweredBy = require('hide-powered-by');
    console.log("CONFIGURING APP");
    // Globbing model files
    let modelPaths = '/app/app/models/**/*.js';
    if (process.env.NODE_ENV === 'local') {
        modelPaths = './app/models/**/*.js';
    }
    config.getGlobbedFiles(modelPaths).forEach(function (modelPath) {
        console.log("MODELS", modelPath);
        require(path.resolve(modelPath));
    });
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(cors());
    app.use(compression());
    app.use(methodOverride());
    app.use(hidePoweredBy());
    queryParser_1.handleQueryParser(app);
    // Set swig as the template engine
    app.engine('server.view.html', consolidate[config.templateEngine]);
    // Set views path and view engine
    app.set('view engine', 'server.view.html');
    app.set('views', './app/views');
    // Setting the app router and static folder
    app.use(express.static(path.resolve('./public')));
    // Globbing routing files
    let routePaths = '/app/app/routes/**/*.js';
    if (process.env.NODE_ENV === 'local') {
        routePaths = './app/routes/**/*.js';
    }
    config.getGlobbedFiles(routePaths).forEach(function (routePath) {
        console.log("ROUTES", routePath);
        require(path.resolve(routePath))(app);
    });
    // if (process.env.NODE_ENV == "production"){
    // 	app.use(raygunClient.expressHandler);
    // }
    return app;
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhwcmVzcy5qcyIsInNvdXJjZVJvb3QiOiJhcHAvIiwic291cmNlcyI6WyJjb25maWcvZXhwcmVzcy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxZQUFZLENBQUM7O0FBR2IsK0RBQWdFO0FBRWhFLElBQUksT0FBTyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFDNUIsVUFBVSxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFDbkMsV0FBVyxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFDcEMsY0FBYyxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxFQUMzQyxRQUFRLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQyxFQUM5QixFQUFFLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUNyQixJQUFJLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUNuQixJQUFJLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUN0QixXQUFXLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxFQUNwQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0FBQzdCLDhCQUE4QjtBQUNqQyxtRkFBbUY7QUFJcEYsTUFBTSxDQUFDLE9BQU8sR0FBRztJQUVoQixJQUFJLEdBQUcsR0FBRyxPQUFPLEVBQUUsQ0FBQztJQUNwQixJQUFJLGFBQWEsR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUMvQyxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDL0IsdUJBQXVCO0lBQ3ZCLElBQUksVUFBVSxHQUFHLHlCQUF5QixDQUFDO0lBQzNDLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEtBQUssT0FBTyxFQUFFO1FBQy9CLFVBQVUsR0FBRyxzQkFBc0IsQ0FBQztLQUMxQztJQUNELE1BQU0sQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVMsU0FBUztRQUM1RCxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxTQUFTLENBQUMsQ0FBQztRQUNqQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO0lBQ2xDLENBQUMsQ0FBQyxDQUFDO0lBR0gsR0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLEVBQUMsUUFBUSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUMsQ0FBQztJQUNqRCxHQUFHLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBQyxLQUFLLEVBQUUsTUFBTSxFQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUNoQixHQUFHLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7SUFDdkIsR0FBRyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDO0lBQzFCLEdBQUcsQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUN6QiwrQkFBaUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUV2QixrQ0FBa0M7SUFDbEMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsRUFBRSxXQUFXLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7SUFFbkUsaUNBQWlDO0lBQ2pDLEdBQUcsQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLGtCQUFrQixDQUFDLENBQUM7SUFDM0MsR0FBRyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUM7SUFDaEMsMkNBQTJDO0lBQzNDLEdBQUcsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUlsRCx5QkFBeUI7SUFDekIsSUFBSSxVQUFVLEdBQUcseUJBQXlCLENBQUM7SUFDeEMsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsS0FBSyxPQUFPLEVBQUU7UUFDbEMsVUFBVSxHQUFHLHNCQUFzQixDQUFDO0tBQ3ZDO0lBQ0osTUFBTSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBUyxTQUFTO1FBQzVELE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQ2pDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDdkMsQ0FBQyxDQUFDLENBQUM7SUFFSCw2Q0FBNkM7SUFDN0MseUNBQXlDO0lBQ3pDLElBQUk7SUFFSixPQUFPLEdBQUcsQ0FBQztBQUVaLENBQUMsQ0FBQyJ9