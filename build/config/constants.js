'use strict';
module.exports = {
    priceKmBike: 0.60,
    priceKmMotorbike: 0.43,
    priceKmCar: 0.535,
    priceKmTruck: 0.66,
    priceKmMotorbikeFar: 0.92,
    priceKmCarFar: 1.33,
    priceKmTruckFar: 1.5,
    priceTransports: {
        bike: 4.5,
        motorbike: 4.32,
        car: 6.18,
        truck: 10
    },
    driverPercentatge: 0.70,
    packagePrice: 2.5
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uc3RhbnRzLmpzIiwic291cmNlUm9vdCI6ImFwcC8iLCJzb3VyY2VzIjpbImNvbmZpZy9jb25zdGFudHMuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsWUFBWSxDQUFDO0FBRWIsTUFBTSxDQUFDLE9BQU8sR0FBRztJQUNoQixXQUFXLEVBQUUsSUFBSTtJQUNqQixnQkFBZ0IsRUFBRSxJQUFJO0lBQ3RCLFVBQVUsRUFBRSxLQUFLO0lBQ2pCLFlBQVksRUFBRSxJQUFJO0lBQ2xCLG1CQUFtQixFQUFFLElBQUk7SUFDekIsYUFBYSxFQUFFLElBQUk7SUFDbkIsZUFBZSxFQUFFLEdBQUc7SUFDcEIsZUFBZSxFQUFFO1FBQ2hCLElBQUksRUFBRSxHQUFHO1FBQ1QsU0FBUyxFQUFFLElBQUk7UUFDZixHQUFHLEVBQUUsSUFBSTtRQUNULEtBQUssRUFBRSxFQUFFO0tBQ1Q7SUFDRCxpQkFBaUIsRUFBRSxJQUFJO0lBQ3ZCLFlBQVksRUFBRSxHQUFHO0NBQ2pCLENBQUMifQ==