"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
/**
 * Module dependencies.
 */
const to_1 = __importDefault(require("./app/tools/to"));
const MongoDBInitializer_1 = require("./app/database/MongoDBInitializer");
const init_1 = require("./app/services/sockets/init");
const DriversModule_1 = require("./app/services/drivers/DriversModule");
const drivers_controller_1 = require("./app/services/drivers/controllers/drivers.controller");
const DriversHttpController_1 = require("./app/services/drivers/httpControllers/DriversHttpController");
const config = require('./config/config');
class App {
    constructor(dbInitializer) {
        this.dbInitializer = dbInitializer;
        this.app = require('./config/express')();
    }
    mountTCPModules(server) {
        return __awaiter(this, void 0, void 0, function* () {
            const s = new init_1.SocketModule(server);
        });
    }
    start() {
        return __awaiter(this, void 0, void 0, function* () {
            const [dbError, connection] = yield to_1.default(this.dbInitializer.initializeDatabase());
            //const [docError, documentation] = await to(this.docInitializer.initializeDocumentation(this.app));
            if (dbError) {
                console.log('Database error', dbError);
            }
            console.log('Database connected');
            /*if (docError) {
                console.log('Documentation error', docError);
            }*/
            console.log('Documentation generated');
            new DriversModule_1.DriversModule().setupRoutes(this.app, new DriversHttpController_1.DriversHttpController(new drivers_controller_1.DriverController()));
            const server = require('http').createServer(this.app);
            this.mountTCPModules(server);
            // Set up port
            const port = process.env.PORT || 3000;
            const environment = process.env.NODE_ENV || 'development';
            //this.app.set('port', process.env.PORT || 3000);
            server.listen(port, () => {
                console.log(`Server is listening on ${port} with environment ${environment}`);
            });
        });
    }
}
exports.App = App;
let app = new App(new MongoDBInitializer_1.MongoDBInitializer());
app.start();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwic291cmNlUm9vdCI6ImFwcC8iLCJzb3VyY2VzIjpbImFwcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFDQTs7R0FFRztBQUNILHdEQUFnQztBQUVoQywwRUFBcUU7QUFDckUsc0RBQXlEO0FBRXpELHdFQUFxRTtBQUNyRSw4RkFBeUY7QUFDekYsd0dBQXFHO0FBR3JHLE1BQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0FBRTFDLE1BQWEsR0FBRztJQUdaLFlBQ1ksYUFBbUM7UUFBbkMsa0JBQWEsR0FBYixhQUFhLENBQXNCO1FBSC9DLFFBQUcsR0FBRyxPQUFPLENBQUMsa0JBQWtCLENBQUMsRUFBRSxDQUFDO0lBTWpDLENBQUM7SUFFRSxlQUFlLENBQUMsTUFBYzs7WUFDaEMsTUFBTSxDQUFDLEdBQUcsSUFBSSxtQkFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBR3ZDLENBQUM7S0FBQTtJQUNLLEtBQUs7O1lBQ1AsTUFBTSxDQUFDLE9BQU8sRUFBRSxVQUFVLENBQUMsR0FBRyxNQUFNLFlBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixFQUFFLENBQUMsQ0FBQztZQUNoRixvR0FBb0c7WUFFcEcsSUFBSSxPQUFPLEVBQUU7Z0JBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxPQUFPLENBQUMsQ0FBQzthQUMxQztZQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQztZQUNsQzs7ZUFFRztZQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUd2QyxJQUFJLDZCQUFhLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLDZDQUFxQixDQUMvRCxJQUFJLHFDQUFnQixFQUFFLENBQ3pCLENBQUMsQ0FBQTtZQUdGLE1BQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3RELElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDN0IsY0FBYztZQUNkLE1BQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQztZQUN0QyxNQUFNLFdBQVcsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsSUFBSSxhQUFhLENBQUM7WUFFMUQsaURBQWlEO1lBRWpELE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRTtnQkFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsSUFBSSxxQkFBcUIsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUNsRixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7S0FBQTtDQUNKO0FBN0NELGtCQTZDQztBQUVELElBQUksR0FBRyxHQUFHLElBQUksR0FBRyxDQUNiLElBQUksdUNBQWtCLEVBQUUsQ0FHM0IsQ0FBQztBQUVGLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyJ9