"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SwaggerDoc = void 0;
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
class SwaggerDoc {
    initializeDocumentation(app) {
        return __awaiter(this, void 0, void 0, function* () {
            let docRoute = "./doc/doc.yaml";
            if (process.env.NODE_ENV !== 'local') {
                docRoute = "app/build/doc/doc.yaml";
            }
            const swaggerDocument = YAML.load(docRoute);
            app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
            return;
        });
    }
}
exports.SwaggerDoc = SwaggerDoc;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3dhZ2dlckRvYy5qcyIsInNvdXJjZVJvb3QiOiJhcHAvIiwic291cmNlcyI6WyJkb2MvU3dhZ2dlckRvYy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSxNQUFNLFNBQVMsR0FBRyxPQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQztBQUNoRCxNQUFNLElBQUksR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7QUFFL0IsTUFBYSxVQUFVO0lBQ2IsdUJBQXVCLENBQUMsR0FBUTs7WUFDbEMsSUFBSSxRQUFRLEdBQUcsZ0JBQWdCLENBQUM7WUFDaEMsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsS0FBSyxPQUFPLEVBQUU7Z0JBQ2xDLFFBQVEsR0FBRyx3QkFBd0IsQ0FBQTthQUN0QztZQUNELE1BQU0sZUFBZSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDNUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7WUFFeEUsT0FBTztRQUNYLENBQUM7S0FBQTtDQUVKO0FBWkQsZ0NBWUMifQ==