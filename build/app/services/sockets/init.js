"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketModule = void 0;
const socket_io_1 = require("socket.io");
const sockets_route_controller_1 = require("./routes/sockets.route.controller");
//import * as SocketIO from "socket.io";
class SocketModule {
    constructor(server) {
        this.print = () => __awaiter(this, void 0, void 0, function* () {
            console.log('aqui yes : ');
        });
        this.setSockets = () => __awaiter(this, void 0, void 0, function* () {
            const socketsRouteController = sockets_route_controller_1.SocketsRouteController.getInstance();
            yield this.print();
            this.io.of('/user').on('connection', (socket) => __awaiter(this, void 0, void 0, function* () {
                console.log(`-------------------------------------------- user ${socket.handshake.query.userId} connected --------------------------------------------`);
                socketsRouteController.putNewSocketToArray(socket);
                //socketsRouteController.sendInfoToAllUsers();
                socket.on('disconnect', () => __awaiter(this, void 0, void 0, function* () {
                    console.log('aqui disconnecting user : ', socket.handshake.query.userId);
                    yield socketsRouteController.disconnectUser(socket);
                }));
            }));
        });
        console.log('inside socket constructor');
        /*let options:SocketIO.ServerOptions = {
            allowUpgrades: true,
            transports: [ 'polling', 'websocket' ],
            pingTimeout: 9000,
            pingInterval: 3000,
            cookie: 'mycookie',
            httpCompression: true,
            origins: '*:*'
    };*/
        this.io = socket_io_1.listen(server);
        this.setSockets();
    }
}
exports.SocketModule = SocketModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5pdC5qcyIsInNvdXJjZVJvb3QiOiJhcHAvIiwic291cmNlcyI6WyJhcHAvc2VydmljZXMvc29ja2V0cy9pbml0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLHlDQUF5QztBQUN6QyxnRkFBeUU7QUFFekUsd0NBQXdDO0FBRXhDLE1BQWEsWUFBWTtJQUlyQixZQUFZLE1BQWM7UUFnQjFCLFVBQUssR0FBRyxHQUFTLEVBQUU7WUFDZixPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBRyxDQUFDO1FBQ2pDLENBQUMsQ0FBQSxDQUFDO1FBRUYsZUFBVSxHQUFJLEdBQVMsRUFBRTtZQUVyQixNQUFNLHNCQUFzQixHQUFHLGlEQUFzQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3BFLE1BQU0sSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ25CLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxZQUFZLEVBQUUsQ0FBTyxNQUFhLEVBQUUsRUFBRTtnQkFDekQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxREFBcUQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSx5REFBeUQsQ0FBQyxDQUFDO2dCQUN6SixzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDbkQsOENBQThDO2dCQUM5QyxNQUFNLENBQUMsRUFBRSxDQUFDLFlBQVksRUFBRyxHQUFTLEVBQUU7b0JBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsNEJBQTRCLEVBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQzFFLE1BQU0sc0JBQXNCLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN4RCxDQUFDLENBQUEsQ0FBQyxDQUFDO1lBRVAsQ0FBQyxDQUFBLENBQUMsQ0FBQztRQUVQLENBQUMsQ0FBQSxDQUFDO1FBbENFLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLENBQUMsQ0FBQTtRQUN4Qzs7Ozs7Ozs7UUFRQTtRQUNKLElBQUksQ0FBQyxFQUFFLEdBQUcsa0JBQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN6QixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFFbEIsQ0FBQztDQXNCSjtBQXhDRCxvQ0F3Q0MifQ==