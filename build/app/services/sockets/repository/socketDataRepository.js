"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketDataRepository = void 0;
const sockets_route_controller_1 = require("../routes/sockets.route.controller");
class SocketDataRepository {
    /* UTIL PER FER EL REFACTOR
    static async setTracking (message:{[key:string]: number;}) {
        const trackingObj = new Tracking();
        const pointObj = new Point();
        const latLongObj = new LatLong();
        latLongObj.latitude = message['latitude'];
        latLongObj.longitude = message['longitude'];
        pointObj.coordinates = latLongObj;
        trackingObj.point = pointObj;
        
        return trackingObj;
    }*/
    static getActuatorEmitModel(driverId, message) {
        //crear els models de location i tal... REFACTOR!
        const driverData = {
            driverId: '',
            location: {
                longitude: 0,
                latitude: 0
            }
        };
        driverData.driverId = driverId;
        driverData.location.latitude = message.location['latitude'] ? message.location['latitude'] : message.location['lat'];
        driverData.location.longitude = message.location['longitude'] ? message.location['longitude'] : message.location['lng'];
        return driverData;
    }
    static socketEmitter(socket, channel, data) {
        return __awaiter(this, void 0, void 0, function* () {
            if (socket !== undefined) {
                try {
                    console.log('emitting (data, channel) : ', data, channel);
                    socket.emit(channel, data);
                }
                catch (error) {
                    console.log('error emitting : ', error);
                }
            }
            else {
                console.log('Socket is undefined, if process is driver_update, probably there is no user connected 👎');
            }
        });
    }
    static socketEmitterOfDriverLocation(data) {
        return __awaiter(this, void 0, void 0, function* () {
            //en userSocket dictionari, iterar per users i pillar el contingut, que es el socket
            console.log('inside socket emitter of driver location');
            const allSockets = sockets_route_controller_1.SocketsRouteController.getInstance().allSockets;
            console.log('aqui all sockets : ', allSockets);
            if (allSockets !== undefined) {
                for (const socket of allSockets) {
                    this.socketEmitter(socket, 'driver_update', data);
                }
            }
            else {
                console.log('there is not any socket connection');
            }
        });
    }
}
exports.SocketDataRepository = SocketDataRepository;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic29ja2V0RGF0YVJlcG9zaXRvcnkuanMiLCJzb3VyY2VSb290IjoiYXBwLyIsInNvdXJjZXMiOlsiYXBwL3NlcnZpY2VzL3NvY2tldHMvcmVwb3NpdG9yeS9zb2NrZXREYXRhUmVwb3NpdG9yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFFQSxpRkFBMEU7QUFFMUUsTUFBYSxvQkFBb0I7SUFJN0I7Ozs7Ozs7Ozs7O09BV0c7SUFHSCxNQUFNLENBQUMsb0JBQW9CLENBQUUsUUFBZSxFQUFHLE9BQVc7UUFDdEQsaURBQWlEO1FBQ2pELE1BQU0sVUFBVSxHQUFzRTtZQUNsRixRQUFRLEVBQUMsRUFBRTtZQUNYLFFBQVEsRUFBQztnQkFDTCxTQUFTLEVBQUMsQ0FBQztnQkFDWCxRQUFRLEVBQUMsQ0FBQzthQUNiO1NBQ0osQ0FBQztRQUNGLFVBQVUsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQy9CLFVBQVUsQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckgsVUFBVSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN4SCxPQUFPLFVBQVUsQ0FBQztJQUN0QixDQUFDO0lBRUQsTUFBTSxDQUFPLGFBQWEsQ0FBRSxNQUFhLEVBQUcsT0FBYyxFQUFHLElBQWdCOztZQUN6RSxJQUFJLE1BQU0sS0FBSyxTQUFTLEVBQUU7Z0JBQ3RCLElBQUc7b0JBQ0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyw2QkFBNkIsRUFBRyxJQUFJLEVBQUcsT0FBTyxDQUFDLENBQUE7b0JBQzNELE1BQU0sQ0FBQyxJQUFJLENBQUUsT0FBTyxFQUFHLElBQUksQ0FBRSxDQUFDO2lCQUNqQztnQkFDRCxPQUFPLEtBQUssRUFBRTtvQkFDVixPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixFQUFHLEtBQUssQ0FBQyxDQUFDO2lCQUM1QzthQUNKO2lCQUFNO2dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsMEZBQTBGLENBQUMsQ0FBQzthQUMzRztRQUVMLENBQUM7S0FBQTtJQUVELE1BQU0sQ0FBTyw2QkFBNkIsQ0FBRSxJQUFPOztZQUMvQyxvRkFBb0Y7WUFDcEYsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQ0FBMEMsQ0FBQyxDQUFDO1lBQ3hELE1BQU0sVUFBVSxHQUFHLGlEQUFzQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQztZQUNuRSxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFHLFVBQVUsQ0FBQyxDQUFDO1lBQ2hELElBQUksVUFBVSxLQUFLLFNBQVMsRUFBRTtnQkFDMUIsS0FBSyxNQUFNLE1BQU0sSUFBSSxVQUFVLEVBQUM7b0JBQzVCLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFDLGVBQWUsRUFBQyxJQUFJLENBQUMsQ0FBQztpQkFDbkQ7YUFDSjtpQkFBTTtnQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLG9DQUFvQyxDQUFDLENBQUE7YUFDcEQ7UUFFTCxDQUFDO0tBQUE7Q0FDSjtBQTlERCxvREE4REMifQ==