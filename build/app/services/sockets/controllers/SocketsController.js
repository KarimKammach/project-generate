"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketsController = void 0;
const socketDataRepository_1 = require("../repository/socketDataRepository");
class SocketsController {
    setClientSocket(socket, clientId, dictionary) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('inside user socket assignation');
            if (!dictionary[clientId]) {
                dictionary[clientId] = [];
            }
            dictionary[clientId].push(socket);
            return dictionary;
        });
    }
    deleteClientSocket(socket, clientId, dictionary) {
        return __awaiter(this, void 0, void 0, function* () {
            let elementPos = dictionary.map((element) => {
                {
                    return element.id;
                }
            }).indexOf(socket.id);
            let socketToDelete = dictionary[elementPos];
            socketToDelete.disconnect();
            if (elementPos > -1)
                dictionary.splice(elementPos, 1);
            console.log(' /************* client with id : ', clientId, ' is now disconnected /*************/');
        });
    }
    unlinkClientSocket(socket, clientId, dictionary) {
        return __awaiter(this, void 0, void 0, function* () {
            delete dictionary[clientId];
            console.log(' /************* client with id : ', clientId, ' is now unlinked /*************/');
        });
    }
    emitter(socket, channel, data) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('aqui emitting data : ', JSON.stringify(data, null, 2));
            socketDataRepository_1.SocketDataRepository.socketEmitter(socket, channel, data);
        });
    }
    checkMessageTimestamp(data) {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
}
exports.SocketsController = SocketsController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU29ja2V0c0NvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiYXBwLyIsInNvdXJjZXMiOlsiYXBwL3NlcnZpY2VzL3NvY2tldHMvY29udHJvbGxlcnMvU29ja2V0c0NvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBR0EsNkVBQTBFO0FBRTFFLE1BQWEsaUJBQWlCO0lBR3BCLGVBQWUsQ0FBRSxNQUFhLEVBQUcsUUFBZSxFQUFHLFVBQW9DOztZQUN6RixPQUFPLENBQUMsR0FBRyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7WUFDOUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBQztnQkFDdEIsVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQzthQUM3QjtZQUNELFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbEMsT0FBTyxVQUFVLENBQUM7UUFDdEIsQ0FBQztLQUFBO0lBS0ssa0JBQWtCLENBQUUsTUFBYyxFQUFFLFFBQWUsRUFBRyxVQUFtQjs7WUFDM0UsSUFBSSxVQUFVLEdBQUcsVUFBVSxDQUFDLEdBQUcsQ0FBRSxDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUN6QztvQkFBQyxPQUFPLE9BQU8sQ0FBQyxFQUFFLENBQUM7aUJBQUU7WUFBQSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQy9DLElBQUksY0FBYyxHQUFHLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUM1QyxjQUFjLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDNUIsSUFBSSxVQUFVLEdBQUcsQ0FBQyxDQUFDO2dCQUFFLFVBQVUsQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3RELE9BQU8sQ0FBQyxHQUFHLENBQUMsbUNBQW1DLEVBQUcsUUFBUSxFQUFHLHNDQUFzQyxDQUFDLENBQUM7UUFFekcsQ0FBQztLQUFBO0lBRUssa0JBQWtCLENBQUUsTUFBYyxFQUFFLFFBQWUsRUFBRyxVQUFrQzs7WUFDMUYsT0FBTyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQ0FBbUMsRUFBRyxRQUFRLEVBQUcsa0NBQWtDLENBQUMsQ0FBQztRQUVyRyxDQUFDO0tBQUE7SUFFSyxPQUFPLENBQUUsTUFBYSxFQUFHLE9BQWMsRUFBRyxJQUFnQjs7WUFDNUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsRUFBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBQyxJQUFJLEVBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuRSwyQ0FBb0IsQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFDLE9BQU8sRUFBQyxJQUFJLENBQUMsQ0FBQztRQUM1RCxDQUFDO0tBQUE7SUFFSyxxQkFBcUIsQ0FBRSxJQUFPOztRQUVwQyxDQUFDO0tBQUE7Q0FDSjtBQXZDRCw4Q0F1Q0MifQ==