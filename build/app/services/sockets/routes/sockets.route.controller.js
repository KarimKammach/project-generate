"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketsRouteController = void 0;
const SocketsController_1 = require("../controllers/SocketsController");
class SocketsRouteController {
    constructor() {
        this.allSockets = [];
        this.socketsController = new SocketsController_1.SocketsController();
    }
    static getInstance() {
        if (!this._instance) {
            this._instance = new SocketsRouteController();
        }
        return SocketsRouteController._instance;
    }
    putNewSocketToArray(socket) {
        return __awaiter(this, void 0, void 0, function* () {
            this.allSockets.push(socket);
            console.log('aqui sockets  : ');
            for (let socket of this.allSockets) {
                console.log(socket.id);
            }
        });
    }
    sendInfoToAllUsers(data) {
        return __awaiter(this, void 0, void 0, function* () {
            for (const socket of this.allSockets) {
                this.socketsController.emitter(socket, 'driver_update', data);
            }
        });
    }
    disconnectUser(socket) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.socketsController.deleteClientSocket(socket, socket.handshake.query.userId, this.allSockets);
            console.log('aqui sockets  : ');
            for (let socket of this.allSockets) {
                console.log(socket.id);
            }
        });
    }
}
exports.SocketsRouteController = SocketsRouteController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic29ja2V0cy5yb3V0ZS5jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6ImFwcC8iLCJzb3VyY2VzIjpbImFwcC9zZXJ2aWNlcy9zb2NrZXRzL3JvdXRlcy9zb2NrZXRzLnJvdXRlLmNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBRUEsd0VBQXFFO0FBRXJFLE1BQWEsc0JBQXNCO0lBTy9CO1FBSEEsZUFBVSxHQUFrQixFQUFFLENBQUM7UUFJM0IsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUkscUNBQWlCLEVBQUUsQ0FBQztJQUNyRCxDQUFDO0lBRUQsTUFBTSxDQUFDLFdBQVc7UUFDZCxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNqQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksc0JBQXNCLEVBQUUsQ0FBQztTQUNqRDtRQUNELE9BQU8sc0JBQXNCLENBQUMsU0FBUyxDQUFDO0lBQzVDLENBQUM7SUFFSyxtQkFBbUIsQ0FBRSxNQUFhOztZQUNwQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUM3QixPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUE7WUFDL0IsS0FBSyxJQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUMxQjtRQUNMLENBQUM7S0FBQTtJQUVJLGtCQUFrQixDQUFFLElBQVE7O1lBQzlCLEtBQUssTUFBTSxNQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDbEMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUMsZUFBZSxFQUFDLElBQUksQ0FBQyxDQUFBO2FBQzlEO1FBQ0wsQ0FBQztLQUFBO0lBR00sY0FBYyxDQUFDLE1BQWE7O1lBQzlCLE1BQU0sSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3hHLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQTtZQUMvQixLQUFLLElBQUksTUFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQzFCO1FBQ0wsQ0FBQztLQUFBO0NBRUo7QUF6Q0Qsd0RBeUNDIn0=