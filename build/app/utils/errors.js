'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
exports.buildErrorResponse = void 0;
function buildErrorResponse(code, message) {
    return {
        errorCode: code,
        message,
    };
}
exports.buildErrorResponse = buildErrorResponse;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXJyb3JzLmpzIiwic291cmNlUm9vdCI6ImFwcC8iLCJzb3VyY2VzIjpbImFwcC91dGlscy9lcnJvcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsWUFBWSxDQUFDOzs7QUFFYixTQUFnQixrQkFBa0IsQ0FBQyxJQUFZLEVBQUUsT0FBZTtJQUM1RCxPQUFPO1FBQ0gsU0FBUyxFQUFFLElBQUk7UUFDZixPQUFPO0tBQ1YsQ0FBQztBQUNOLENBQUM7QUFMRCxnREFLQyJ9