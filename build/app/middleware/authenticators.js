"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleJwtAuth = void 0;
const jwt = __importStar(require("jsonwebtoken"));
const errors_1 = require("../utils/errors");
const bson_1 = require("bson");
const freePaths = [
    '/status',
    '/login',
    '/user',
    '/doc',
    '/login-actuator',
];
class DecodedJWT {
}
const authenticate = (req, res, next) => {
    const isGetUser = req.path === '/user' && (req.method === 'GET' || req.method === 'PATCH');
    if (freePaths.includes(req.path) && !isGetUser)
        return next();
    if (req.headers['authorization'] === undefined || req.headers['authorization'] == null || req.headers['authorization'] === "")
        return res.status(400).json(errors_1.buildErrorResponse("BR001", "Missing here headers in the request"));
    try {
        const decoded = jwt.verify(req.headers.authorization, process.env.JWT_SECRET || 'secret');
        if (decoded.sub === undefined)
            return res.status(401).send(errors_1.buildErrorResponse("BR002", "Invalid authentication"));
        req.body["userId"] = decoded.sub;
        req.body["userid"] = decoded.sub;
        req.body["createdBy"] = new bson_1.ObjectId(decoded.sub);
        next();
    }
    catch (e) {
        return res.status(401).send(errors_1.buildErrorResponse("BR002", "Invalid authentication"));
    }
};
exports.handleJwtAuth = (router) => {
    router.use(authenticate);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRvcnMuanMiLCJzb3VyY2VSb290IjoiYXBwLyIsInNvdXJjZXMiOlsiYXBwL21pZGRsZXdhcmUvYXV0aGVudGljYXRvcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLGtEQUFvQztBQUVwQyw0Q0FBbUQ7QUFDbkQsK0JBQThCO0FBRzlCLE1BQU0sU0FBUyxHQUFHO0lBQ2QsU0FBUztJQUNULFFBQVE7SUFDUixPQUFPO0lBQ1AsTUFBTTtJQUNOLGlCQUFpQjtDQUNwQixDQUFDO0FBRUYsTUFBTSxVQUFVO0NBRWY7QUFFRCxNQUFNLFlBQVksR0FBRyxDQUFDLEdBQVksRUFBRSxHQUFhLEVBQUUsSUFBZ0IsRUFBRSxFQUFFO0lBQ25FLE1BQU0sU0FBUyxHQUFHLEdBQUcsQ0FBQyxJQUFJLEtBQUssT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sS0FBSyxLQUFLLElBQUksR0FBRyxDQUFDLE1BQU0sS0FBSyxPQUFPLENBQUMsQ0FBQztJQUMzRixJQUFJLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUztRQUFFLE9BQU8sSUFBSSxFQUFFLENBQUM7SUFDOUQsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxLQUFLLFNBQVMsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxJQUFJLElBQUksSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUU7UUFBRSxPQUFPLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLDJCQUFrQixDQUFDLE9BQU8sRUFBRSxxQ0FBcUMsQ0FBQyxDQUFDLENBQUM7SUFDL04sSUFBSTtRQUNBLE1BQU0sT0FBTyxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLElBQUksUUFBUSxDQUFlLENBQUM7UUFDeEcsSUFBSSxPQUFPLENBQUMsR0FBRyxLQUFLLFNBQVM7WUFBRSxPQUFPLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLDJCQUFrQixDQUFDLE9BQU8sRUFBRSx3QkFBd0IsQ0FBQyxDQUFDLENBQUM7UUFDbEgsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBSSxPQUFPLENBQUMsR0FBRyxDQUFDO1FBQ2xDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQztRQUNsQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFJLElBQUksZUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNuRCxJQUFJLEVBQUUsQ0FBQztLQUNWO0lBQUMsT0FBTyxDQUFDLEVBQUU7UUFDUixPQUFPLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLDJCQUFrQixDQUFDLE9BQU8sRUFBRSx3QkFBd0IsQ0FBQyxDQUFDLENBQUM7S0FDdEY7QUFDTCxDQUFDLENBQUM7QUFHVyxRQUFBLGFBQWEsR0FBRyxDQUFDLE1BQWMsRUFBRSxFQUFFO0lBQzVDLE1BQU0sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUM7QUFDN0IsQ0FBQyxDQUFDIn0=