'use strict';
/**
 * Module dependencies.
 */
var config = require('../../config/env/' + (process.env.NODE_ENV || 'local'));
module.exports = function (app) {
    app.use(function (req, res, next) {
        // Website you wish to allow to connect
        var allowedOrigins = ['https://www.shargo.io', 'https://shargo.io', 'http://185.34.193.226', 'http://142.93.224.80:9011',
            'http://142.93.224.80:9011/home/comercial/clientServices'];
        if (process.env.NODE_ENV == "production") {
            var origin = req.headers.origin;
            if (allowedOrigins.indexOf(origin) > -1) {
                res.setHeader('Access-Control-Allow-Origin', origin);
            }
        }
        else {
            res.setHeader('Access-Control-Allow-Origin', '*');
        }
        // Request headers you wish to allow
        // Request headers you wish to allow
        res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Origin, Authorization, Origin, x-requested-with, Content-Type, Content-Range, Content-Disposition, Content-Description, x-token, x-userid");
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE");
        next();
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWlkZGxld2FyZXMuanMiLCJzb3VyY2VSb290IjoiYXBwLyIsInNvdXJjZXMiOlsiYXBwL21pZGRsZXdhcmUvbWlkZGxld2FyZXMuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsWUFBWSxDQUFDO0FBRWI7O0dBRUc7QUFDSCxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsbUJBQW1CLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDO0FBRTlFLE1BQU0sQ0FBQyxPQUFPLEdBQUcsVUFBUyxHQUFHO0lBRTVCLEdBQUcsQ0FBQyxHQUFHLENBQUMsVUFBUyxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUk7UUFDOUIsdUNBQXVDO1FBQ3ZDLElBQUksY0FBYyxHQUFHLENBQUMsdUJBQXVCLEVBQUUsbUJBQW1CLEVBQUUsdUJBQXVCLEVBQUcsMkJBQTJCO1lBQ3pILHlEQUF5RCxDQUFDLENBQUM7UUFFM0QsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsSUFBSSxZQUFZLEVBQUU7WUFDekMsSUFBSSxNQUFNLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUM7WUFDaEMsSUFBRyxjQUFjLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFDO2dCQUNwQyxHQUFHLENBQUMsU0FBUyxDQUFDLDZCQUE2QixFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQ3ZEO1NBQ0Q7YUFBSTtZQUNKLEdBQUcsQ0FBQyxTQUFTLENBQUMsNkJBQTZCLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FHbEQ7UUFFRCxvQ0FBb0M7UUFFakMsb0NBQW9DO1FBQ3BDLEdBQUcsQ0FBQyxTQUFTLENBQUMsOEJBQThCLEVBQUUsZ0tBQWdLLENBQUMsQ0FBQztRQUNoTixHQUFHLENBQUMsU0FBUyxDQUFDLDhCQUE4QixFQUFFLHdCQUF3QixDQUFDLENBQUM7UUFDeEUsSUFBSSxFQUFFLENBQUM7SUFFWCxDQUFDLENBQUMsQ0FBQztBQUdKLENBQUMsQ0FBQyJ9