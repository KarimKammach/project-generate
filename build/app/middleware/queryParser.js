"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleQueryParser = void 0;
const qs_1 = __importDefault(require("qs"));
/**
 * Parses the originar query.params into a usable filter object for the app to consume
 * @param req
 * @param res
 * @param next
 */
const parseQuery = (req, res, next) => {
    if (req.query.q) {
        req.query.q = buildQueryObject(req.query.q);
    }
    else {
        req.query.q = {};
    }
    if (req.query.params) {
        req.query.params = buildQueryObject(req.query.params);
    }
    else {
        req.query.params = {};
    }
    if (req.query.fields) {
        req.query.fields = buildProjectionObject(req.query.fields);
    }
    else {
        req.query.fields = {};
    }
    if (req.query.sortBy) {
        req.query.sortBy = buildSortingsObject(req.query.sortBy);
    }
    else {
        req.query.sortBy = {};
    }
    next();
};
const buildQueryObject = (queryString) => {
    queryString = queryString.replace(/::/gi, '=');
    queryString = queryString.replace(/,/gi, '&');
    return qs_1.default.parse(queryString);
};
const buildProjectionObject = (queryString) => {
    queryString = queryString.replace(/::/gi, '=');
    queryString = queryString.replace(/,/gi, '&');
    return qs_1.default.parse(queryString);
};
const buildSortingsObject = (queryString) => {
    let sortingsObject = {};
    let items = queryString.split(',');
    for (let item of items) {
        let sign = item[0];
        let key = item.slice(1);
        console.log('sign : ', sign, ' key :', key);
        sortingsObject[key] = sign === '-' ? -1 : +1;
    }
    return sortingsObject;
};
exports.handleQueryParser = (router) => {
    router.use(parseQuery);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicXVlcnlQYXJzZXIuanMiLCJzb3VyY2VSb290IjoiYXBwLyIsInNvdXJjZXMiOlsiYXBwL21pZGRsZXdhcmUvcXVlcnlQYXJzZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQ0EsNENBQW9CO0FBR3BCOzs7OztHQUtHO0FBQ0gsTUFBTSxVQUFVLEdBQUcsQ0FBQyxHQUFZLEVBQUUsR0FBYSxFQUFFLElBQWdCLEVBQUUsRUFBRTtJQUNqRSxJQUFJLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO1FBQ2IsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFXLENBQUMsQ0FBQztLQUN6RDtTQUFNO1FBQ0gsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO0tBQ3BCO0lBQ0QsSUFBSSxHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRTtRQUNsQixHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLE1BQWdCLENBQUMsQ0FBQztLQUNuRTtTQUFNO1FBQ0gsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO0tBQ3pCO0lBQ0QsSUFBSSxHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRTtRQUNsQixHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxxQkFBcUIsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLE1BQWdCLENBQUMsQ0FBQztLQUN4RTtTQUFNO1FBQ0gsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO0tBQ3pCO0lBQ0QsSUFBSSxHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRTtRQUNsQixHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLE1BQWdCLENBQUMsQ0FBQztLQUN0RTtTQUFNO1FBQ0gsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO0tBQ3pCO0lBQ0QsSUFBSSxFQUFFLENBQUM7QUFDWCxDQUFDLENBQUM7QUFFRixNQUFNLGdCQUFnQixHQUFHLENBQUMsV0FBbUIsRUFBRSxFQUFFO0lBQzdDLFdBQVcsR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQztJQUMvQyxXQUFXLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDOUMsT0FBTyxZQUFFLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0FBQ2pDLENBQUMsQ0FBQztBQUVGLE1BQU0scUJBQXFCLEdBQUcsQ0FBQyxXQUFtQixFQUFFLEVBQUU7SUFDbEQsV0FBVyxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQy9DLFdBQVcsR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztJQUM5QyxPQUFPLFlBQUUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7QUFDakMsQ0FBQyxDQUFDO0FBRUYsTUFBTSxtQkFBbUIsR0FBRyxDQUFDLFdBQW1CLEVBQUUsRUFBRTtJQUNoRCxJQUFJLGNBQWMsR0FBd0IsRUFBRSxDQUFDO0lBQzdDLElBQUksS0FBSyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDbkMsS0FBSyxJQUFJLElBQUksSUFBSSxLQUFLLEVBQUU7UUFDcEIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25CLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUcsSUFBSSxFQUFFLFFBQVEsRUFBSSxHQUFHLENBQUMsQ0FBQztRQUMvQyxjQUFjLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQ2hEO0lBQ0QsT0FBTyxjQUFjLENBQUM7QUFDMUIsQ0FBQyxDQUFDO0FBRVcsUUFBQSxpQkFBaUIsR0FBRyxDQUFDLE1BQWMsRUFBRSxFQUFFO0lBQ2hELE1BQU0sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7QUFDM0IsQ0FBQyxDQUFDIn0=