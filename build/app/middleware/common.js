"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleCompression = exports.handleBodyRequestParsing = exports.handleCors = void 0;
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const compression_1 = __importDefault(require("compression"));
exports.handleCors = (router) => router.use(cors_1.default({ credentials: true, origin: true }));
exports.handleBodyRequestParsing = (router) => {
    router.use(body_parser_1.default.urlencoded({ extended: true }));
    router.use(body_parser_1.default.json());
};
exports.handleCompression = (router) => {
    router.use(compression_1.default());
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLmpzIiwic291cmNlUm9vdCI6ImFwcC8iLCJzb3VyY2VzIjpbImFwcC9taWRkbGV3YXJlL2NvbW1vbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFDQSxnREFBd0I7QUFDeEIsOERBQWlDO0FBQ2pDLDhEQUF1QztBQUUxQixRQUFBLFVBQVUsR0FBRyxDQUFDLE1BQWMsRUFBRSxFQUFFLENBQ3pDLE1BQU0sQ0FBQyxHQUFHLENBQUMsY0FBSSxDQUFDLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO0FBRTdDLFFBQUEsd0JBQXdCLEdBQUcsQ0FBQyxNQUFjLEVBQUUsRUFBRTtJQUN2RCxNQUFNLENBQUMsR0FBRyxDQUFDLHFCQUFNLENBQUMsVUFBVSxDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNsRCxNQUFNLENBQUMsR0FBRyxDQUFDLHFCQUFNLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztBQUM5QixDQUFDLENBQUM7QUFFVyxRQUFBLGlCQUFpQixHQUFHLENBQUMsTUFBYyxFQUFFLEVBQUU7SUFDaEQsTUFBTSxDQUFDLEdBQUcsQ0FBQyxxQkFBVyxFQUFFLENBQUMsQ0FBQztBQUM5QixDQUFDLENBQUMifQ==