"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("./common");
const authenticators_1 = require("./authenticators");
exports.default = [common_1.handleCors, common_1.handleBodyRequestParsing, common_1.handleCompression, authenticators_1.handleJwtAuth];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiYXBwLyIsInNvdXJjZXMiOlsiYXBwL21pZGRsZXdhcmUvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxxQ0FJa0I7QUFFbEIscURBRTBCO0FBSTFCLGtCQUFlLENBQUMsbUJBQVUsRUFBRSxpQ0FBd0IsRUFBRSwwQkFBaUIsRUFBRSw4QkFBYSxDQUFDLENBQUMifQ==