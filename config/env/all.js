'use strict';

module.exports = {
	app: {
		title: 'Shargo',
		description: 'Sending packages super fast',
		keywords: 'Shargo, packages'
	},
	port: process.env.PORT || 8000,
	securePort: 443,
	templateEngine: 'swig',
	secret: 'LAfang.2015PB'
};