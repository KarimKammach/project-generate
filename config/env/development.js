'use strict';

module.exports = {
	db: 'mongodb://shargo:test1234@db.shargo.io/shargo',
	sauronUrl: "https://sauron-sandbox.shargo.io",
	apiUrl: "https://api-sandbox.shargo.io",
	cronUrl: "https://cron-sandbox.shargo.io",
	webUrl: "https://web-sandbox.shargo.io",
	app: {
		title: 'Shargo - Development Environment'
	},
	payment: {
		merchantId: 	"22ygpbzkc37bvsqc",
		publicKey: 		"x6q6tg37pyhgwd6b",
		privateKey: 	"3f5dcf8906deb9acab5392308bf51c62"
	},
	emitter: {
		isEnabled: false,
		url: "http://data-recruiter.shargo.io:5500",
		auth: {
			user: "shargo-data-server",
			pass: "pack3tb3ll.15"
		}
	}
};
