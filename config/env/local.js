'use strict';

module.exports = {
	db: 'mongodb://shargo:test1234@128.199.38.91:37017/shargo',
	sauronUrl: "https://sauron-local.shargo.io",
	apiUrl: "https://api-local.shargo.io",
	cronUrl: "https://cron-local.shargo.io",
	webUrl: "https://web-local.shargo.io",
	app: {
		title: 'Shargo - Development Environment'
	},
	payment: {
		merchantId: 	"22ygpbzkc37bvsqc",
		publicKey: 		"x6q6tg37pyhgwd6b",
		privateKey: 	"3f5dcf8906deb9acab5392308bf51c62"
	},
	emitter: {
		isEnabled: false,
		url: "http://data-recruiter.shargo.io:5500",
		auth: {
			user: "shargo-data-server",
			pass: "pack3tb3ll.15"
		}
	}
};
