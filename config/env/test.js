'use strict';

module.exports = {
	db: 'mongodb://localhost:30000/shargo-test',
	sauronUrl: "https://sauron-test.shargo.io",
	apiUrl: "https://api-test.shargo.io",
	cronUrl: "https://cron-test.shargo.io",
	webUrl: "https://web-test.shargo.io",
	port: 3001,
	app: {
		title: 'Shargo - Test Environment'
	},
	payment: {
		merchantId: 	"22ygpbzkc37bvsqc",
		publicKey: 		"x6q6tg37pyhgwd6b",
		privateKey: 	"3f5dcf8906deb9acab5392308bf51c62"
	},
	emitter: {
		isEnabled: false,
		url: "http://data-recruiter.shargo.io:5500",
		auth: {
			user: "shargo-data-server",
			pass: "pack3tb3ll.15"
		}
	}
};
