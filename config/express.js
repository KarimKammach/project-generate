'use strict';


import {handleQueryParser} from "../app/middleware/queryParser";

var	express = require('express'),
    bodyParser = require('body-parser'),
    compression = require('compression'),
    methodOverride = require('method-override'),
    mongoose = require('mongoose'),
    fs = require('fs'),
	cors = require('cors'),
    path = require('path'),
    consolidate = require('consolidate'),
    config = require('./config');
    // raygun = require('raygun'),
	// raygunClient = new raygun.Client().init({ apiKey: 'g3QH5dkSIFHk5WmQQ/uquQ==' });



module.exports = function() {

	var app = express();
	var hidePoweredBy = require('hide-powered-by');
	console.log("CONFIGURING APP");
	// Globbing model files
	let modelPaths = '/app/app/models/**/*.js';
	if (process.env.NODE_ENV === 'local') {
        modelPaths = './app/models/**/*.js';
	}
	config.getGlobbedFiles(modelPaths).forEach(function(modelPath) {
		console.log("MODELS", modelPath);
		require(path.resolve(modelPath));
	});

	
	app.use(bodyParser.urlencoded({extended: true}));
	app.use(bodyParser.json({limit: '50mb'}));
	app.use(cors());
	app.use(compression());
	app.use(methodOverride());
	app.use(hidePoweredBy());
	handleQueryParser(app);

	// Set swig as the template engine
	app.engine('server.view.html', consolidate[config.templateEngine]);

	// Set views path and view engine
	app.set('view engine', 'server.view.html');
	app.set('views', './app/views');
	// Setting the app router and static folder
	app.use(express.static(path.resolve('./public')));



	// Globbing routing files
	let routePaths = '/app/app/routes/**/*.js';
    if (process.env.NODE_ENV === 'local') {
        routePaths = './app/routes/**/*.js';
    }
	config.getGlobbedFiles(routePaths).forEach(function(routePath) {
		console.log("ROUTES", routePath);
		require(path.resolve(routePath))(app);
	});

	// if (process.env.NODE_ENV == "production"){
	// 	app.use(raygunClient.expressHandler);
	// }

	return app;

};
