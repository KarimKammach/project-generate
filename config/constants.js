'use strict';

module.exports = {
	priceKmBike: 0.60,
	priceKmMotorbike: 0.43,
	priceKmCar: 0.535,
	priceKmTruck: 0.66,
	priceKmMotorbikeFar: 0.92,
	priceKmCarFar: 1.33,
	priceKmTruckFar: 1.5,
	priceTransports: {
		bike: 4.5,
		motorbike: 4.32,
		car: 6.18,
		truck: 10
	},
	driverPercentatge: 0.70,
	packagePrice: 2.5
};
