
/**
 * Module dependencies.
 */
import to from './app/tools/to';
import {IDatabaseInitializer} from './app/database/IDatabaseInitializer';
import {MongoDBInitializer} from './app/database/MongoDBInitializer';
import {SocketModule} from './app/services/sockets/init';
import { Server } from 'https';
import { DriversModule } from './app/services/drivers/DriversModule';
import { DriverController } from './app/services/drivers/controllers/drivers.controller';
import { DriversHttpController } from './app/services/drivers/httpControllers/DriversHttpController';
import { SwaggerDoc } from '@doc/SwaggerDoc';

const config = require('./config/config');

export class App {
    app = require('./config/express')();

    constructor(
        private dbInitializer: IDatabaseInitializer,
        //private docInitializer: IDocInitializer,

    ) {}

    async mountTCPModules(server: Server) {
        const s = new SocketModule(server);
        

    }
    async start() {
        const [dbError, connection] = await to(this.dbInitializer.initializeDatabase());
        //const [docError, documentation] = await to(this.docInitializer.initializeDocumentation(this.app));

        if (dbError) {
            console.log('Database error', dbError);
        }
        console.log('Database connected');
        /*if (docError) {
            console.log('Documentation error', docError);
        }*/
        console.log('Documentation generated');
       

        new DriversModule().setupRoutes(this.app, new DriversHttpController(
            new DriverController()
        ))
      

        const server = require('http').createServer(this.app);
        this.mountTCPModules(server);
        // Set up port
        const port = process.env.PORT || 3000;
        const environment = process.env.NODE_ENV || 'development';
        
        //this.app.set('port', process.env.PORT || 3000);

        server.listen(port, () => {
            console.log(`Server is listening on ${port} with environment ${environment}`);
        });
    }
}

let app = new App(
    new MongoDBInitializer(),
    //new SwaggerDoc()
    
);

app.start();

