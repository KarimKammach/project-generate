'use strict';

export function buildErrorResponse(code: string, message: string) {
    return {
        errorCode: code,
        message,
    };
}
